package com.example.examenc2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {
    private EditText editTextBomba, editTextPrecio, editTextCapacidad, editTextContador;
    private RadioGroup radioGroupGasolina;
    private Button buttonIniciarVenta, btnVenta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextBomba = findViewById(R.id.editTextBomba);
        editTextCapacidad = findViewById(R.id.editTextCapacidad);
        editTextContador = findViewById(R.id.editTextContador);
        editTextPrecio = findViewById(R.id.editTextPrecio);
        radioGroupGasolina = findViewById(R.id.radioGroupGasolina);
        buttonIniciarVenta = findViewById(R.id.buttonIniciarVenta);
        btnVenta = findViewById(R.id.btnVenta);

        buttonIniciarVenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numeroBomba = Integer.parseInt(editTextBomba.getText().toString());
                String tipoGasolina = ((RadioButton) findViewById(radioGroupGasolina.getCheckedRadioButtonId())).getText().toString();
                double precio = Double.parseDouble(editTextPrecio.getText().toString());
                double capacidad = Double.parseDouble(editTextCapacidad.getText().toString());
                double contadorLitros = Double.parseDouble(editTextContador.getText().toString());

            }
        });

        btnVenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numeroBomba = Integer.parseInt(editTextBomba.getText().toString());
                String tipoGasolina = ((RadioButton) findViewById(radioGroupGasolina.getCheckedRadioButtonId())).getText().toString();
                double capacidadBomba = 200.0;
                int contadorVentas = 0;

                BombaGasolina bombaGasolina = new BombaGasolina(numeroBomba, tipoGasolina, capacidadBomba, contadorVentas);
            }
        });
    }
}