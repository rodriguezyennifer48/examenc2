package com.example.examenc2;

public class BombaGasolina {
    int numeroBomba;
    String tipoGasolina;
    double capacidadBomba;
    int contadorVentas;
    public BombaGasolina(int numeroBomba, String tipoGasolina, double capacidadBomba, int contadorVentas) {
        this.numeroBomba = 0;
        this.capacidadBomba = 0.0;
        this.contadorVentas = 0;
        this.tipoGasolina = "";

    }

    public int getNumeroBomba(){
        return numeroBomba;
    }

    public void setNumeroBomba(int numeroBomba){
        this.numeroBomba = numeroBomba;
    }

    public String getTipoGasolina(){
        return tipoGasolina;
    }

    public void setTipoGasolina(String tipoGasolina){
        this.tipoGasolina = tipoGasolina;
    }

    public double getCapacidadBomba(){
        return capacidadBomba;
    }

    public void setCapacidadBomba(double capacidadBomba){
        this.capacidadBomba = capacidadBomba;
    }

    public int getContadorVentas(){
        return contadorVentas;
    }

    public void setContadorVentas(int contadorVentas){
        this.contadorVentas = contadorVentas;
    }
}
